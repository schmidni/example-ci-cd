from app import decrease_counter, increase_counter, r


def test_counter():
    r.set('counter', 0)
    increase_counter()
    assert r.get('counter') == '1'
    decrease_counter()
    assert r.get('counter') == '0'
