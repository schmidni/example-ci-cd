import os

from dotenv import load_dotenv
from flask import Flask
from redis import Redis

# load environment variables from dotenv
load_dotenv()

# get environment variables or default
REDIS_PASS = os.getenv('REDIS_PASSWORD')
REDIS_HOST = os.getenv('REDIS_HOST') or 'localhost'
REDIS_PORT = os.getenv('REDIS_PORT') or 6379

# initialize flask app
app = Flask(__name__)

# initialize redis client
r = Redis(host=REDIS_HOST, password=REDIS_PASS,
          port=REDIS_PORT, decode_responses=True)

# initialize counter key to 0 in redis
r.set('counter', 0)


def increase_counter():
    """Increase counter by 1"""
    r.set('counter', int(r.get('counter')) + 1)


def decrease_counter():
    """Decrease counter by 1"""
    r.set('counter', int(r.get('counter')) - 1)


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/increase")
def increase():
    increase_counter()
    return f"<p>Counter: {r.get('counter')}</p>"


@app.route("/decrease")
def decrease():
    decrease_counter()
    return f"<p>Counter: {r.get('counter')}</p>"


@app.route("/reset")
def reset():
    r.set('counter', 0)
    return f"<p>Counter: {r.get('counter')}</p>"
