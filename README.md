# Local Development
Start a local Redis server:
```
docker run --name redis -p 6379:6379 -d redis:latest
```

Install Python and Environment:
```
python3.10 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

Test Application:
```
flake8 --exclude=env/ . -v 
pytest tests.py
```

Run Flask Application
```
flask run
```


>The Source code of the Flask Application and the Dockerfile should not be taken as examples on how to write a Flask Application or how to write a Dockerfile. 
>The Flask Application and the Dockerfile are only there to show how to set up a CI/CD pipeline with GitLab.


# Local Setup with Docker
It's possible to manually set up the Docker containers locally:

```
# Create .env file
echo "REDIS_HOST=redis" > .env
```
```
# Set up Docker containers
docker network create example_network
docker build -t example .
docker run --name example --network example_network -p 5000:5000 -d example
docker run --name redis -p 6379:6379 --network example_network -d redis:latest
```
```
# Tear down Docker containers
docker stop example
docker rm example
docker stop redis
docker rm redis
docker network rm example_network
rm .env
```

# Local Setup with Docker Compose
It's also possible to use Docker Compose to set up the Docker containers locally:

First you need to uncomment the `build` option and comment the `image` option in the `docker-compose.yml` file.

```
  flask:
    build: .
    # image: ${TAG_COMMIT}
```

Then you can run the following commands:

```
docker compose build
docker compose up -d
``````

# Environment Variables for CI/CD
```
REDIS_HOST (should be set to "redis" for all)
REDIS_PORT (should be set to "6379" for all)
REDIS_PASSWORD (should only be set for deployment environment)

SERVER_RSA (file) (SSH key to connect to respective deployment environment)
SERVER_USER (SSH user to connect to respective deployment environment)
```

Also, a `file` environment CI/CD Variable should be created, which pulls together the Redis Variables:

```
REDIS_HOST=$REDIS_HOST
REDIS_PORT=$REDIS_PORT
REDIS_PASSWORD=$REDIS_PASSWORD
```
The option "Expand variable reference" should be checked for this file.

# CI
Make changes, push to the `main` branch and the `flake8` and `pytest` tests will run automatically.

or

Go to the `Build`-> `Pipelines` page and press the button `Run Pipeline`. Then select the `main` branch and press the button `Run Pipeline`.

# Environments for CD
For the deployment a `<name>-environment` branch should be created. As well as under `operate` -> `Environments` a new environment should be created with the name `<name>-environment` and the server http address for deployment.

# CD
Make changes, push to the `main` branch, merge those changes to the `<name>-environment` branch, and the changes will be deployed to the VM automatically.

or

Go to the `Build`-> `Pipelines` page and press the button `Run Pipeline`. Then select the `<name>-environment` branch and press the button `Run Pipeline`.

or 

Go to `Operate` -> `Environments` -> `<name>-environment` and select the commit to redeploy / rollback. This is only possible if the commit was deployed successfully before.
